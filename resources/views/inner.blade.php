@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center inner">
            <div class="col-md-12">
                <div class="mx-3 my-5">
                {!! $page->post !!}
                </div>
            </div>
        </div>
    </div>
@endsection
