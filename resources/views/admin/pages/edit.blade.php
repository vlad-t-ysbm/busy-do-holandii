@extends('layouts.admin-app')

@section('content')
    <form method="post" action="{{route('pages.update', $page->id)}}">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="exampleFormControlInput1">Page title</label>
            <input class="form-control" name="title" type="text" placeholder="Page title" value="{{$page->title}}">
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">Slug</label>
            <input class="form-control" name="slug" type="text" placeholder="Slug" value="{{$page->slug}}">
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">Page parrent</label>
            <select class="form-control" name="parent_id">
                <option value="0">None</option>
                @foreach($pages as $item)
                    @if($page->parent_id == $item->id)
                        <option value="{{$item->id}}" selected>{{$item->title}}</option>
                    @else
                        <option value="{{$item->id}}">{{$item->title}}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Post</label>
            <textarea class="form-control editor" id="exampleFormControlTextarea1" name="post" rows="5">{!!$page->post!!}</textarea>
        </div>
        <button type="submit" class="btn btn-info">Save</button>
    </form>
@endsection
