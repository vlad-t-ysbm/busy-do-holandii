@extends('layouts.admin-app')

@section('content')
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Page title</th>
            <th scope="col">Page url</th>
            <th scope="col">Post</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($pages as $key => $value)
        <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$value->title}}</td>
            <td>{{$value->slug}}</td>
            <td>{{$value->post}}</td>
            <td class="d-flex">
                <a class="btn btn-warning action-button" href="{{route('pages.edit', $value->id)}}">Edit</a>
                <form method="post" action="{{route('pages.destroy', $value->id)}}">
                    @csrf
                    @method('delete')
                    <button class="btn btn-danger action-button ml-2" type="submit">Delete</button>
                </form>

            </td>
{{--            <td></td>--}}
        </tr>
        @endforeach
        </tbody>
    </table>
@endsection
