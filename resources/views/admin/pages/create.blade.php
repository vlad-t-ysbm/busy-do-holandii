@extends('layouts.admin-app')

@section('content')
<form method="post" action="{{route('pages.store')}}">
    @csrf
    <div class="form-group">
        <label for="exampleFormControlInput1">Page title</label>
        <input class="form-control" name="title" type="text" placeholder="Page title">
    </div>
    <div class="form-group">
        <label for="exampleFormControlInput1">Slug</label>
        <input class="form-control" name="slug" type="text" placeholder="Slug">
    </div>
    <div class="form-group">
        <label for="exampleFormControlInput1">Page parrent</label>
        <select class="form-control" name="parent_id">
            <option value="0">None</option>
            @foreach($pages as $page)
            <option value="{{$page->id}}">{{$page->title}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Post</label>
        <textarea class="form-control post" id="exampleFormControlTextarea1" name="post" rows="5"></textarea>
    </div>
    <button type="submit" class="btn btn-info">Create</button>
</form>

@endsection
