<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            @foreach( $pages as $page)
                @if($page->children()->count())
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="{{route('page', $page->slug)}}" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{$page->title}}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            @foreach($page->children() as $subpage)
                                <a class="dropdown-item" href="{{route('page', $subpage->slug)}}">{{$subpage->title}}</a>
                            @endforeach
                        </div>
                    </li>
                @else
                <li class="nav-item active">
                    <a class="nav-link" href="{{route('page', $page->slug)}}">{{$page->title}}<span class="sr-only">(current)</span></a>
                </li>
                @endif
            @endforeach
        </ul>
    </div>
</nav>

