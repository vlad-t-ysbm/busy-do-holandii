<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Blog\BlogController@index');
Route::get('/page/{page}', 'Blog\BlogController@show')->name('page');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/register', 'Admin\AdminController@redirect');
Route::get('/password/reset', 'Admin\AdminController@redirect');
Route::get('/admin', 'Admin\AdminController@admin');

Route::group(['middleware'=>'auth', 'prefix'=>'admin'],function () {
//    Route::get('/create', 'Admin\PagesController@create')->name('create-page');
//    Route::post('/create-page', 'Admin\PagesController@store');
    Route::resource('/pages', 'Admin\PagesController');
});



